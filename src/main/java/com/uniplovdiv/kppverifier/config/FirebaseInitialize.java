package com.uniplovdiv.kppverifier.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

@Service
public class FirebaseInitialize {
	
	@PostConstruct
	public void initialize() {
		
		try {
			FileInputStream serviceAccount = new FileInputStream("./firebaseServiceAccount.json");
			FirebaseOptions options = new FirebaseOptions.Builder()
					  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
					  .setDatabaseUrl("https://kpp-document-verifier.firebaseio.com")
					  .build();

			FirebaseApp.initializeApp(options);
		} catch (FileNotFoundException e) {
			System.out.println("Problem with finding firebase config key");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Problem with reading from firebase database");
			e.printStackTrace();
		}
				
	}
}
