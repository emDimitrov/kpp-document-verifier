package com.uniplovdiv.kppverifier.controllers;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.firebase.internal.FirebaseService;
import com.uniplovdiv.kppverifier.entities.User;
import com.uniplovdiv.kppverifier.services.UserService;

@RestController
public class TestController {
	
	private UserService userService;
	
	@Autowired
    public TestController(UserService userService) {
		this.userService = userService;
    }
	
	@PostMapping("/api/register")
    public String register(@RequestBody User user) throws InterruptedException, ExecutionException{
		return userService.createUser(user);
    }
	
	@PostMapping("api/getUser")
    public User register(@RequestHeader String egn) throws InterruptedException, ExecutionException{
		return userService.getUser(egn);
    }
	
	@PutMapping("/api/updateUser")
	public String updateUser(@RequestBody User user) throws InterruptedException, ExecutionException {
		return userService.updateUser(user);
	}
	
	@DeleteMapping("/api/deleteUser")
	public String deleteUser(@RequestHeader String egn) throws InterruptedException, ExecutionException {
		return userService.deleteUser(egn);
	}
}
