package com.uniplovdiv.kppverifier.controllers;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.uniplovdiv.kppverifier.entities.User;
import com.uniplovdiv.kppverifier.services.UserService;

@Controller
public class UserController extends BaseController {

	private UserService userService;
	
	@Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public ModelAndView getLogin() {
        return this.view("views/user/login",null,"Login");
    }

    @GetMapping("/register")
    public ModelAndView getRegister(){
        return this.view("views/user/register");
    }

    @PostMapping("/register")
    public ModelAndView register(@ModelAttribute User user){
        try {
			this.userService.createUser(user);
		} catch (InterruptedException | ExecutionException e) {
			return this.view("views/user/register");
		}

        return this.view("views/user/login");
    }

}

