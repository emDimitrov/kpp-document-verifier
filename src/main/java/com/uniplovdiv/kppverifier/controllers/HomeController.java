package com.uniplovdiv.kppverifier.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.uniplovdiv.kppverifier.services.ImageReaderService;

import net.sourceforge.tess4j.TesseractException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class HomeController extends BaseController{
	
	private ImageReaderService readerService;
	
	@Autowired
	public HomeController(ImageReaderService readerService) {
		this.readerService=readerService;
	}
    @GetMapping("/")
    public ModelAndView index() throws TesseractException{
    	this.readerService.getIdentifier();
        return this.view("views/index", null,"Welcome");
    }

    @GetMapping("/home")
    public ModelAndView home() {
        return this.view("views/home", "","Resident Evil");
    }
}

