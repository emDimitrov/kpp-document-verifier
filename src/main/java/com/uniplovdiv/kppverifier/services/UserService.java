package com.uniplovdiv.kppverifier.services;

import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import com.uniplovdiv.kppverifier.entities.User;

@Service
public class UserService {
	
	public String createUser(User user) throws InterruptedException, ExecutionException {
		Firestore dbFirestore = FirestoreClient.getFirestore();
		ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection("users").document(user.getEgn()).set(user);
				
		return collectionsApiFuture.get().getUpdateTime().toString();
	}
	
	public User getUser(String egn) throws InterruptedException, ExecutionException {
		Firestore dbFirestore = FirestoreClient.getFirestore();
		DocumentReference documentReference = dbFirestore.collection("users").document(egn);
		ApiFuture<DocumentSnapshot> future = documentReference.get();
		
		DocumentSnapshot document = future.get();
		
		User user = null;
		if(document.exists()) {
			user = document.toObject(User.class);
		}
		
		return user;
	}
	
	public String updateUser(User user) throws InterruptedException, ExecutionException {
		Firestore dbFirestore = FirestoreClient.getFirestore();
		ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection("users").document(user.getEgn()).set(user);
				
		return collectionsApiFuture.get().getUpdateTime().toString();
	}
	
	public String deleteUser(String egn) throws InterruptedException, ExecutionException {
		Firestore dbFirestore = FirestoreClient.getFirestore();
		ApiFuture<WriteResult> writeResult = dbFirestore.collection("users").document(egn).delete();
		
		return "Document with ID " + egn + " was deleted";
	}

}
