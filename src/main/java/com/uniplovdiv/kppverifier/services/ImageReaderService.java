package com.uniplovdiv.kppverifier.services;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

@Service
public class ImageReaderService {
	
	@Autowired private ResourceLoader resourceLoader;

	  
	public String getIdentifier() throws TesseractException {
		String inputFilePath = "./test.jpg";
		Tesseract tesseract = new Tesseract();
		tesseract.setDatapath("./tessdata/eng.traineddata");
		String fullText = tesseract.doOCR(new File(inputFilePath));
		
		System.out.println(fullText);
		return fullText;
	}
}
