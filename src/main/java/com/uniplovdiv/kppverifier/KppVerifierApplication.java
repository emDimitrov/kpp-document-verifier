package com.uniplovdiv.kppverifier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KppVerifierApplication {

	public static void main(String[] args) {
		SpringApplication.run(KppVerifierApplication.class, args);
	}

}
